<?php
/*
Plugin Name: empoderacine.com CORE
Plugin URI: https://empoderacine.com/
Description: Plugin del core de empoderacine.com
Version: 1.0
Author: Flavia Bernárdez
Author URI: https://flabernardez.com/
License: GPLv2 or later
Text Domain: emc-core
*/

function fla_soporte_mimes($mime_types = array()){

	$mime_types['svg'] = 'image/svg+xml';
	$mime_types['zip'] = 'application/zip';
	return $mime_types;

}

add_filter('upload_mimes', 'fla_soporte_mimes');